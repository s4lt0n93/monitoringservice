﻿using System;
using Microsoft.AspNetCore.Mvc;
using PT.Monitoring.Cache;
using PT.Monitoring.Models;
using PT.Monitoring.Settings;

namespace PT.Monitoring.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class MonitoringController : ControllerBase
    {
        private readonly IAgentsCache _agentsCache;
        private readonly HealthStatusSettings _healthStatusSettings;

        public MonitoringController(IAgentsCache agentsCache, HealthStatusSettings healthStatusSettings)
        {
            if (agentsCache == null) throw new ArgumentNullException(nameof(agentsCache));
            if (healthStatusSettings == null) throw new ArgumentNullException(nameof(healthStatusSettings));
            _agentsCache = agentsCache;
            _healthStatusSettings = healthStatusSettings;
        }

        [HttpGet]
        public ActionResult GetHealth()
        {
            return Content(_healthStatusSettings.HealthStatusOk);
        }

        [HttpPost]
        public void SendStatus([FromBody] AgentModelRequest agentModelRequest)
        {
            _agentsCache.AddOrUpdate(agentModelRequest.AgentId, agentModelRequest.Errors);
        }
    }
}