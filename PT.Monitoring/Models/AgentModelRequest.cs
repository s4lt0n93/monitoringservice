﻿using System.ComponentModel.DataAnnotations;

namespace PT.Monitoring.Models
{
    public class AgentModelRequest
    {
        [Required]
        public string AgentId { get; set; }
        public string[] Errors { get; set; }
    }
}
