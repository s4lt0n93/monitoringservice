﻿using System;

namespace PT.Monitoring.Models
{
    public class Agent
    {
        public string AgentId { get; set; }
        public string[] Errors { get; set; }

        public virtual bool IsActive { get => LastActivityTime > DateTime.UtcNow.AddSeconds(-30); }

        public DateTime LastActivityTime { get; set; }
    }
}
