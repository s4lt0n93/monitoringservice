﻿using System.Collections.Generic;
using PT.Monitoring.Models;

namespace PT.Monitoring.Cache
{
    public interface IAgentsCache
    {
        void AddOrUpdate(string agentId, string[] errors);

        IEnumerable<Agent> GetAll();

    }
}
