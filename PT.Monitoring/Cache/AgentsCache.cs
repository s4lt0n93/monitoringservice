﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using PT.Monitoring.Models;
[assembly: InternalsVisibleTo("PT.Monitoring.Test")]

namespace PT.Monitoring.Cache
{
    internal class AgentsCache : IAgentsCache
    {
        private readonly ConcurrentDictionary<string, Agent> _cache = new ConcurrentDictionary<string, Agent>();

        public void AddOrUpdate(string agentId, string[] errors)
        {
            var agent = new Agent
            {
                AgentId = agentId,
                Errors = errors ?? new string[0],
                LastActivityTime = DateTime.UtcNow
            };

            _cache.AddOrUpdate(agentId, agent, (updatingAgentId, updatingAgent) =>
            {
                updatingAgent.LastActivityTime = agent.LastActivityTime;
                updatingAgent.Errors = agent.Errors;
                return updatingAgent;
            });
        }

        public IEnumerable<Agent> GetAll()
        {
            return _cache.Values;
        }
    }
}
