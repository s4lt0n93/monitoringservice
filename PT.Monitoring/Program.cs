﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using StructureMap.AspNetCore;

namespace PT.Monitoring
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var config = new ConfigurationBuilder()
                .AddJsonFile("hosting.json")
                .Build();

            WebHost.CreateDefaultBuilder(args)
                .UseConfiguration(config)
                .UseStructureMap()
                .UseStartup<Startup>()
                .Build()
                .Run();
        }
    }
}