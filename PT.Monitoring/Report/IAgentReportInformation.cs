﻿using PT.Monitoring.Models;

namespace PT.Monitoring.Report
{
    public interface IAgentReportInformation
    {
        string GetAgentInfo(Agent agent);
    }
}
