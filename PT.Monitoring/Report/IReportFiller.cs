﻿using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace PT.Monitoring.Report
{
    public interface IReportFiller
    {
        Task FillReport(TextWriter report, CancellationToken cancellationToken);
    }
}
