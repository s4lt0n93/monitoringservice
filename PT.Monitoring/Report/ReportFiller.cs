﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using PT.Monitoring.Cache;

namespace PT.Monitoring.Report
{
    internal class ReportFiller : IReportFiller
    {
        private readonly IAgentsCache _agentsCache;
        private readonly IAgentReportInformation _agentReportInformation;

        public ReportFiller(IAgentsCache agentsCache, IAgentReportInformation agentReportFormatter)
        {
            if (agentsCache == null) throw new ArgumentNullException(nameof(agentsCache));
            if (agentReportFormatter == null) throw new ArgumentNullException(nameof(agentReportFormatter));
            _agentsCache = agentsCache;
            _agentReportInformation = agentReportFormatter;
        }

        public async Task FillReport(TextWriter report, CancellationToken cancellationToken)
        {
            var agents = _agentsCache.GetAll();

            foreach (var agent in agents)
            {
                var agentInfo = _agentReportInformation.GetAgentInfo(agent);
                await report.WriteAsync(agentInfo);
            }
        }
    }
}
