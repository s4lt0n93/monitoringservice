﻿using System.Threading;
using System.Threading.Tasks;

namespace PT.Monitoring.Report
{
    public interface IReportSaver
    {
        Task Save(CancellationToken cancelationToken);
    }
}
