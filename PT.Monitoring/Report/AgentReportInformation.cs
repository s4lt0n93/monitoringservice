﻿using System;
using System.Text;
using PT.Monitoring.Models;

namespace PT.Monitoring.Report
{
    internal class AgentReportInformation : IAgentReportInformation
    {
        public string GetAgentInfo(Agent agent)
        {
            var stringBuilder = new StringBuilder()
                .Append($"Agent Id: {agent.AgentId} ")
                .AppendLine($"Last activity: {agent.LastActivityTime} UTC ")
                .Append($"Status: {(agent.IsActive ? "Active" : "Inactive")} ");

            if (agent.IsActive)
            {
                if (agent.Errors.Length > 0)
                {
                    stringBuilder.Append(" with errors: ").AppendLine().AppendJoin(", ", agent.Errors);
                }
                else
                {
                    stringBuilder.Append(" without errors");
                }
            }
            else
            {
                var seconds = Convert.ToInt32((DateTime.UtcNow - agent.LastActivityTime).TotalSeconds);
                stringBuilder.Append($" within {seconds} seconds");
            }

            stringBuilder.AppendLine().AppendLine();

            return stringBuilder.ToString();
        }


    }
}
