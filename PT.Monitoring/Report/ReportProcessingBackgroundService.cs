﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using PT.Monitoring.Settings;

namespace PT.Monitoring.Report
{
    internal sealed class ReportProcessingBackgroundService : BackgroundService
    {
        private readonly ReportSettings _reportSettings;
        private readonly IReportFiller _reportCreator;
        private readonly IReportSaver _reportSaver;

        public ReportProcessingBackgroundService(ReportSettings reportSettings, IReportFiller reportCreator, IReportSaver reportSaver)
        {
            if (reportSettings == null) throw new ArgumentNullException(nameof(reportSettings));
            if (reportCreator == null) throw new ArgumentNullException(nameof(reportCreator));
            if (reportSaver == null) throw new ArgumentNullException(nameof(reportSaver));
            _reportSettings = reportSettings;
            _reportCreator = reportCreator;
            _reportSaver = reportSaver;
        }

        protected override async Task ExecuteAsync(CancellationToken cancelationToken)
        {
            while (!cancelationToken.IsCancellationRequested)
            {
                await Task.Delay(_reportSettings.SleepPeriodInMs, cancelationToken);
                try
                {
                    await _reportSaver.Save(cancelationToken);
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

        }
    }
}
