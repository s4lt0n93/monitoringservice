﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using PT.Monitoring.Settings;
[assembly: InternalsVisibleTo("PT.Monitoring.Test")]

namespace PT.Monitoring.Report
{
    internal class ReportSaver : IReportSaver
    {
        private readonly ReportSettings _reportSettings;
        private readonly IReportFiller _reportCreator;

        public ReportSaver(ReportSettings reportSettings, IReportFiller reportCreator)
        {
            if (reportSettings == null) throw new ArgumentNullException(nameof(reportSettings));
            if (reportCreator == null) throw new ArgumentNullException(nameof(reportCreator));
            _reportSettings = reportSettings;
            _reportCreator = reportCreator;
        }

        public async Task Save(CancellationToken cancelationToken)
        {
            Directory.CreateDirectory(_reportSettings.Path);

            using (TextWriter report = File.CreateText(FileName))
            {
                await _reportCreator.FillReport(report, cancelationToken);
            }
        }

        private string FileName => $"{_reportSettings.Path}/Report_{DateTime.Now.ToString("yyyy_MM_dd_HH_mm")}_{Guid.NewGuid()}.txt";
    }
}
