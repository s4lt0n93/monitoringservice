﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PT.Monitoring.Cache;
using PT.Monitoring.Controllers;
using PT.Monitoring.Report;
using PT.Monitoring.Settings;
using StructureMap;

namespace PT.Monitoring
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2).AddApplicationPart(typeof(MonitoringController).Assembly);
            services.AddHostedService<ReportProcessingBackgroundService>();

        }

        public void ConfigureContainer(Registry registry)
        {
            registry.For<ReportSettings>().Add(Configuration.GetSection("Report").Get<ReportSettings>());
            registry.For<HealthStatusSettings>().Add(Configuration.GetSection("HealthStatus").Get<HealthStatusSettings>());
            registry.For<IReportSaver>().Use<ReportSaver>().Singleton();
            registry.For<IReportFiller>().Use<ReportFiller>().Singleton();
            registry.For<IAgentsCache>().Use<AgentsCache>().Singleton();
            registry.For<IAgentReportInformation>().Use<AgentReportInformation>().Singleton();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
