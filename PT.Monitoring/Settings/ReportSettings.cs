﻿namespace PT.Monitoring.Settings
{
    public class ReportSettings
    {
        public string Path { get; set; }
        public int SleepPeriodInMs { get; set; }
    }
}
