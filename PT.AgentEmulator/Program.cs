﻿using System;
using System.IO;
using Microsoft.Extensions.Configuration;
using PT.AgentEmulator.Agent;
using PT.AgentEmulator.AgentEmulator;
using PT.AgentEmulator.Settings;
using StructureMap;

namespace PT.AgentEmulator
{
    internal sealed class Program
    {
        private static IConfiguration _configuration;
        private static IContainer _container;

        private static void Main(string[] args)
        {
            BuildConfiguration();
            BuildContainer();
            InitAgentsmulation();

            do
            {
                Console.WriteLine("Press ESC to exit");
            }
            while (Console.ReadKey().Key != ConsoleKey.Escape);

            Environment.Exit(0);
        }

        private static void InitAgentsmulation()
        {
            int maxAgents = _container.GetInstance<ProgramSettings>().MaxAgents;
            int agentsCount;
            do
            {
                Console.WriteLine($"Enter the number of agents (every agent is separate thread, so enter count greater than 0  and than or equal to {maxAgents}):");
            }
            while (!int.TryParse(Console.ReadLine(), out agentsCount) || (agentsCount > maxAgents || agentsCount <= 0));

            RunAgentEmulation(agentsCount);
        }

        private static void RunAgentEmulation(int agentsCount)
        {
            var agentsEmulationService = _container.GetInstance<IAgentEmulatorService>();
            agentsEmulationService.SetAgentsCount(agentsCount);
            agentsEmulationService.SeedAgents();
        }

        private static void BuildConfiguration()
        {
            var builder = new ConfigurationBuilder()
                            .SetBasePath(Directory.GetCurrentDirectory())
                            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            _configuration = builder.Build();
        }

        private static void BuildContainer()
        {
            _container = new Container();
            _container.Configure(config =>
            {
                config.For<AgentSettings>().Add(new AgentSettings { DelayTimeInMs = int.Parse(_configuration.GetSection("AgentSettings:DelayTimeInMs").Value) });
                config.For<ApiSettings>().Add(new ApiSettings { Url = _configuration.GetSection("Api:Url").Value });
                config.For<ProgramSettings>().Add(new ProgramSettings { MaxAgents = int.Parse(_configuration.GetSection("Program:MaxAgents").Value) });
                config.For<IAgentEmulatorService>().Use<AgentEmulatorService>().Singleton();
                config.For<IAgentEntity>().Use<Agent.AgentEntity>().AlwaysUnique();
                config.For<AgentFactory>().Singleton();
            });
        }
    }
}
