﻿using System;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using PT.AgentEmulator.Helpers;
using PT.AgentEmulator.Settings;
[assembly: InternalsVisibleTo("PT.AgentEmulator.Test")]

namespace PT.AgentEmulator.Agent
{
    internal class AgentEntity : IAgentEntity
    {
        private readonly ApiSettings _apiSettings;
        private readonly AgentSettings _agentSettings;
        private int Id { get; set; }
        private string[] Errors => ErrorsGenerator.GenerateErrors();

        public AgentEntity(ApiSettings apiSettings, AgentSettings agentSettings)
        {
            if (apiSettings == null) throw new ArgumentNullException(nameof(apiSettings));
            if (agentSettings == null) throw new ArgumentException(nameof(agentSettings));
            _apiSettings = apiSettings;
            _agentSettings = agentSettings;
        }

        public async Task SendStatus()
        {
            var httpContent = new StringContent(JsonSerializer.Serialize(GetAgentModelRequest()), Encoding.UTF8, "application/json");

            using (var httpClient = new HttpClient())
            {
                await httpClient.PostAsync(_apiSettings.Url, httpContent);
            }
        }

        public void SetAgentNumber(int number)
        {
            Id = number;
        }

        public void StartSendingStatus()
        {
            Task.Factory.StartNew(async () =>
            {
                while (true)
                {
                    await Task.Delay(Id * _agentSettings.DelayTimeInMs);
                    await SendStatus();
                }
            });
        }

        private AgentModelRequest GetAgentModelRequest() => new AgentModelRequest { AgentId = Id.ToString(), Errors = Errors };
    }
}