﻿using StructureMap;

namespace PT.AgentEmulator.Agent
{
    internal sealed class AgentFactory
    {
        private readonly IContainer _container;

        public AgentFactory(IContainer container)
        {
            _container = container;
        }

        public IAgentEntity CreateAgent(int number)
        {
            var agent = _container.GetInstance<IAgentEntity>();
            agent.SetAgentNumber(number);
            return agent;
        }
    }
}
