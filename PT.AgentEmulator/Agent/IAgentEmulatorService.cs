﻿namespace PT.AgentEmulator.AgentEmulator
{
    public interface IAgentEmulatorService
    {
        void SetAgentsCount(int agentsCount);

        void SeedAgents();
    }
}
