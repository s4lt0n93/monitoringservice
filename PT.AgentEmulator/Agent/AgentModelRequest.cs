﻿namespace PT.AgentEmulator.Agent
{
    public class AgentModelRequest
    {
        public string AgentId { get; set; }
        public string[] Errors { get; set; }
    }
}
