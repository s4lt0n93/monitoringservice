﻿using System.Threading.Tasks;

namespace PT.AgentEmulator.Agent
{
    public interface IAgentEntity
    {
        void SetAgentNumber(int number);

        Task SendStatus();

        void StartSendingStatus();
    }
}
