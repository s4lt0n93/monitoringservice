﻿using System;
using System.Collections.Concurrent;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using PT.AgentEmulator.Agent;
[assembly: InternalsVisibleTo("PT.AgentEmulator.Test")]

namespace PT.AgentEmulator.AgentEmulator
{
    internal class AgentEmulatorService : IAgentEmulatorService
    {
        private int _agentCount;
        private readonly AgentFactory _agentFactory;
        private readonly ConcurrentBag<IAgentEntity> _agents = new ConcurrentBag<IAgentEntity>();

        public AgentEmulatorService(AgentFactory agentFactory)
        {
            if (agentFactory == null) throw new ArgumentNullException(nameof(agentFactory));
            _agentFactory = agentFactory;
        }

        public void SeedAgents()
        {
            Parallel.For(1, _agentCount + 1, (i) =>
            {
                var agent = _agentFactory.CreateAgent(i);
                agent.StartSendingStatus();
                _agents.Add(agent);
            });
        }

        public void SetAgentsCount(int count)
        {
            if (count <= 0) throw new ArgumentException($"The value of {nameof(count)} must be greater than 0");
            _agentCount = count;
        }
    }
}