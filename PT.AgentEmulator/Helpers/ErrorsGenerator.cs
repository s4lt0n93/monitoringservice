﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
[assembly: InternalsVisibleTo("PT.AgentEmulator.Test")]

namespace PT.AgentEmulator.Helpers
{
    internal static class ErrorsGenerator
    {
        #region ErrorList

        private static readonly IReadOnlyList<string> _errors = new List<string>
        {
            "APC_INDEX_MISMATCH", "DEVICE_QUEUE_NOT_BUSY", "INVALID_AFFINITY_SET", "INVALID_DATA_ACCESS_TRAP",
            "INVALID_PROCESS_ATTACH_ATTEMPT", "INVALID_PROCESS_DETACH_ATTEMPT", "INVALID_SOFTWARE_INTERRUPT",
            "IRQL_NOT_DISPATCH_LEVEL", "IRQL_NOT_GREATER_OR_EQUAL", "IRQL_NOT_LESS_OR_EQUAL", "NO_EXCEPTION_HANDLING_SUPPORT",
            "MAXIMUM_WAIT_OBJECTS_EXCEEDED", "MUTEX_LEVEL_NUMBER_VIOLATION", "NO_USER_MODE_CONTEXT", "SPIN_LOCK_ALREADY_OWNED",
            "SPIN_LOCK_NOT_OWNED", "THREAD_NOT_MUTEX_OWNER", "TRAP_CAUSE_UNKNOWN", "EMPTY_THREAD_REAPER_LIST",
            "CREATE_DELETE_LOCK_NOT_LOCKED", "LAST_CHANCE_CALLED_FROM_KMODE", "CID_HANDLE_CREATION", "CID_HANDLE_DELETION",
            "REFERENCE_BY_POINTER", "BAD_POOL_HEADER", "MEMORY_MANAGEMENT", "PFN_SHARE_COUNT", "PFN_REFERENCE_COUNT",
            "NO_SPIN_LOCK_AVAILABLE", "KMODE_EXCEPTION_NOT_HANDLED", "SHARED_RESOURCE_CONV_ERROR", "KERNEL_APC_PENDING_DURING_EXIT",
            "QUOTA_UNDERFLOW", "FILE_SYSTEM", "FAT_FILE_SYSTEM", "NTFS_FILE_SYSTEM", "NPFS_FILE_SYSTEM", "CDFS_FILE_SYSTEM",
            "RDR_FILE_SYSTEM", "CORRUPT_ACCESS_TOKEN", "SECURITY_SYSTEM", "INCONSISTENT_IRP", "PANIC_STACK_SWITCH", "PORT_DRIVER_INTERNAL",
            "SCSI_DISK_DRIVER_INTERNAL", "DATA_BUS_ERROR", "INSTRUCTION_BUS_ERROR", "SET_OF_INVALID_CONTEXT", "PHASE0_INITIALIZATION_FAILED",
            "PHASE1_INITIALIZATION_FAILED", "UNEXPECTED_INITIALIZATION_CALL", "CACHE_MANAGER", "NO_MORE_IRP_STACK_LOCATIONS",
            "DEVICE_REFERENCE_COUNT_NOT_ZERO", "FLOPPY_INTERNAL_ERROR", "SERIAL_DRIVER_INTERNAL", "SYSTEM_EXIT_OWNED_MUTEX",
            "SYSTEM_UNWIND_PREVIOUS_USER", "SYSTEM_SERVICE_EXCEPTION", "INTERRUPT_UNWIND_ATTEMPTED", "INTERRUPT_EXCEPTION_NOT_HANDLED",
            "MULTIPROCESSOR_CONFIGURATION_NOT_SUPPORTED", "NO_MORE_SYSTEM_PTES", "TARGET_MDL_TOO_SMALL", "MUST_SUCCEED_POOL_EMPTY",
            "ATDISK_DRIVER_INTERNAL", "NO_SUCH_PARTITION", "MULTIPLE_IRP_COMPLETE_REQUESTS", "INSUFFICIENT_SYSTEM_MAP_REGS",
            "DEREF_UNKNOWN_LOGON_SESSION", "REF_UNKNOWN_LOGON_SESSION", "CANCEL_STATE_IN_COMPLETED_IRP", "PAGE_FAULT_WITH_INTERRUPTS_OFF",
            "IRQL_GT_ZERO_AT_SYSTEM_SERVICE", "STREAMS_INTERNAL_ERROR", "FATAL_UNHANDLED_HARD_ERROR", "NO_PAGES_AVAILABLE",
            "PFN_LIST_CORRUPT", "NDIS_INTERNAL_ERROR", "PAGE_FAULT_IN_NONPAGED_AREA", "REGISTRY_ERROR", "MAILSLOT_FILE_SYSTEM",
            "NO_BOOT_DEVICE", "LM_SERVER_INTERNAL_ERROR", "DATA_COHERENCY_EXCEPTION", "INSTRUCTION_COHERENCY_EXCEPTION",
            "XNS_INTERNAL_ERROR", "FTDISK_INTERNAL_ERROR", "PINBALL_FILE_SYSTEM", "CRITICAL_SERVICE_FAILED",
            "SET_ENV_VAR_FAILED", "HAL_INITIALIZATION_FAILED", "UNSUPPORTED_PROCESSOR", "OBJECT_INITIALIZATION_FAILED",
            "SECURITY_INITIALIZATION_FAILED", "PROCESS_INITIALIZATION_FAILED", "HAL1_INITIALIZATION_FAILED",
            "OBJECT1_INITIALIZATION_FAILED", "SECURITY1_INITIALIZATION_FAILED", "SYMBOLIC_INITIALIZATION_FAILED"
        };

        #endregion

        private static readonly int _maximumErrorsCount = _errors.Count;

        public static string[] GenerateErrors()
        {
            var random = new Random();
            var countOfGeneratedErrors = random.Next(0, _maximumErrorsCount);

            if (countOfGeneratedErrors % 2 == 0)
            {
                return new string[0];
            }

            string[] generatedErrors = new string[countOfGeneratedErrors];

            HashSet<int> tookIds = new HashSet<int>();
            int counter = 0;
            do
            {
                var id = random.Next(0, _maximumErrorsCount - 1);
                if (!tookIds.Contains(id))
                {
                    tookIds.Add(id);
                    generatedErrors[counter] = _errors[id];
                    counter++;
                }

            } while (counter != countOfGeneratedErrors);

            return generatedErrors;
        }
    }
}
