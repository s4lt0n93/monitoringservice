﻿using System.IO;
using System.Threading;
using System.Threading.Tasks;
using AutoFixture;
using Microsoft.Extensions.Configuration;
using Moq;
using NUnit.Framework;
using PT.Monitoring.Cache;
using PT.Monitoring.Models;
using PT.Monitoring.Report;
using PT.Monitoring.Settings;

namespace PT.Monitoring.Test
{
    public class ReportSavingTests
    {
        [Test]
        public async Task ReportSavingIntoFolderTest()
        {
            var fixture = new Fixture();

            var configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build();

            var absolutePath = configuration.GetSection("Report").Get<ReportSettings>().Path + "\\TestReports";

            if (!string.IsNullOrEmpty(absolutePath) && Directory.Exists(absolutePath))
            {
                Directory.Delete(absolutePath, true);
            }
            else if (string.IsNullOrEmpty(absolutePath))
            {
                Assert.Fail("File path is empty");
            }

            Directory.CreateDirectory(absolutePath);

            var filesCountBeforeSave = Directory.GetFiles(absolutePath).Length;
            var settings = new ReportSettings { Path = absolutePath };

            var agentCache = new Mock<IAgentsCache>();
            agentCache.Setup(x => x.GetAll())
                .Returns(fixture.CreateMany<Agent>(10));

            var agentReportInformation = new Mock<IAgentReportInformation>();
            agentReportInformation.Setup(x => x.GetAgentInfo(fixture.Create<Agent>()))
                .Returns("sampleText");

            var reportCreator = new ReportFiller(agentCache.Object, agentReportInformation.Object);

            var reportSaver = new ReportSaver(settings, reportCreator);

            await reportSaver.Save(CancellationToken.None);

            var filesCountAfterSave = Directory.GetFiles(absolutePath).Length;

            Assert.AreNotSame(filesCountBeforeSave, filesCountAfterSave);
            Directory.Delete(absolutePath, true);
        }
    }
}
