﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using NUnit.Framework;
using PT.Monitoring.Settings;
using StructureMap;
using StructureMap.AspNetCore;

namespace PT.Monitoring.Test
{
    public class Smoke
    {
        [Test]
        public async Task AgentMonitoringServiceStartsCorrectly()
        {
            var configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build();

            var webHostBuilder = new WebHostBuilder()
                .UseStructureMap()
                .UseConfiguration(configuration)
                .UseStartup<TestStartup>();

            using (var server = new TestServer(webHostBuilder))
            {
                var container = server.Host.Services.GetService(typeof(IContainer)) as IContainer;
                string okStatusMessage = container.GetInstance<HealthStatusSettings>().HealthStatusOk;

                using (var client = server.CreateClient())
                {
                    var response = await client.GetAsync("/api/Monitoring/GetHealth");
                    var healthCheckResult = await response.Content.ReadAsStringAsync();
                    if (healthCheckResult != okStatusMessage)
                    {
                        Assert.Fail();
                    }
                }
            }
            Assert.Pass();
        }

        private class TestStartup : Startup
        {
            public TestStartup(IConfiguration configuration) : base(configuration) { }
        }
    }

}
