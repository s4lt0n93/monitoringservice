﻿using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using PT.Monitoring.Cache;
using PT.Monitoring.Models;
using PT.Monitoring.Report;

namespace PT.Monitoring.Test
{
    public class ReportFillerTests
    {
        [Test]
        [TestCase("sampleText")]
        [TestCase("asdasdasdsadas\r\n")]
        [TestCase("\n\nGGGG\n\n\n\tragentIsOkOrNotIDunnoLol")]
        public async Task ReportFillTest(string expetingText)
        {
            var agentCache = new Mock<IAgentsCache>();
            agentCache.Setup(x => x.GetAll())
                .Returns(new Agent[1] { null });

            var agentReportInformation = new Mock<IAgentReportInformation>();
            agentReportInformation.Setup(x => x.GetAgentInfo(null))
                .Returns(expetingText);

            var reportFiller = new ReportFiller(agentCache.Object, agentReportInformation.Object);

            var stringBuilder = new StringBuilder();
            var tw = new StringWriter(stringBuilder);

            await reportFiller.FillReport(tw, CancellationToken.None);

            Assert.AreEqual(expetingText, stringBuilder.ToString());
        }
    }
}
