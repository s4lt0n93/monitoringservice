﻿using System.Collections.Generic;
using System.Linq;
using AutoFixture;
using NUnit.Framework;
using PT.Monitoring.Cache;
using PT.Monitoring.Models;

namespace PT.Monitoring.Test
{
    public class AgentsCacheTests
    {

        private readonly IList<Agent> _agentsForSeeding = new List<Agent>();
        private const int _agentsCount = 500;

        [SetUp]
        public void Setup()
        {
            var fixture = new Fixture();
            for (int i = 1; i < _agentsCount + 1; i++)
            {
                _agentsForSeeding.Add(fixture.Create<Agent>());
            }
        }

        [Test]
        public void AddIntoAgentCacheTest()
        {
            var agentCache = GetAgentsCacheWithEmptyErrors();

            Assert.AreEqual(agentCache.GetAll().Count(), _agentsForSeeding.Count);
        }

        [Test]
        public void UpdateAgentCacheTest()
        {
            var agentCache = GetAgentsCacheWithEmptyErrors();
            var stringArrayForUpdating = new string[1] { "Updated" };

            int agentCountBeforeUpdating = agentCache.GetAll().Count();
            foreach (var agent in _agentsForSeeding)
            {
                agentCache.AddOrUpdate(agent.AgentId, stringArrayForUpdating);
            }

            int agentCountAfterUpdating = agentCache.GetAll().Count();

            var seededAgentIds = _agentsForSeeding.Select(x => x.AgentId).ToHashSet();

            bool updatedWithNewErrorsCorrectly = agentCache.GetAll()
                .All(agent => agent.Errors == stringArrayForUpdating && seededAgentIds.Contains(agent.AgentId));

            bool countChangedAfterUpdating = agentCountBeforeUpdating != agentCountAfterUpdating;

            Assert.IsTrue(updatedWithNewErrorsCorrectly && !countChangedAfterUpdating);
        }

        private AgentsCache GetAgentsCacheWithEmptyErrors()
        {
            var agentCache = new AgentsCache();

            foreach (var agent in _agentsForSeeding)
            {
                agentCache.AddOrUpdate(agent.AgentId, agent.Errors);
            }

            return agentCache;
        }

    }
}
