using System;
using AutoFixture;
using Moq;
using NUnit.Framework;
using PT.AgentEmulator.Agent;
using PT.AgentEmulator.AgentEmulator;
using StructureMap;

namespace PT.AgentEmulator.Test
{
    public class SetAgentsCountInAgentEmulatorServiceTests
    {
        [Test]
        public void AgentCreatingWithoutCorrectInjectionsTest()
        {
            var testDelegate = new TestDelegate(() => { new AgentEntity(null, null); });
            Assert.Throws(typeof(ArgumentNullException), testDelegate);
        }

        [Test]
        public void SetAgentsCountInAgentEmulatorServiceTest()
        {
            var fixture = new Fixture();
            var agentEntity = fixture.Create<AgentEntity>();

            var containerMock = new Mock<IContainer>();
            containerMock.Setup(x => x.GetInstance<IAgentEntity>()).Returns(agentEntity);

            var agentEmulatorService = new AgentEmulatorService(new AgentFactory(containerMock.Object));
            var testDelegate = new TestDelegate(() => { agentEmulatorService.SetAgentsCount(-1); });

            Assert.Throws(typeof(ArgumentException), testDelegate);
        }
    }
}