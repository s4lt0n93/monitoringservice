﻿using System;
using NUnit.Framework;
using PT.AgentEmulator.Agent;

namespace PT.AgentEmulator.Test
{
    public class AgentTests
    {
        [Test]
        public void AgentCreatingWithoutCorrectInjectionsTest()
        {
            var testDelegate = new TestDelegate(() => { new AgentEntity(null, null); });
            Assert.Throws(typeof(ArgumentNullException), testDelegate);
        }
    }
}
