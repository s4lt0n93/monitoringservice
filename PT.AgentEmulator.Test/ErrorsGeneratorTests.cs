﻿using NUnit.Framework;
using PT.AgentEmulator.Helpers;

namespace PT.AgentEmulator.Test
{
    public class ErrorsGeneratorTests
    {
        [Test]
        [TestCase(300)]
        public void ErrorsGeneratorTest(int iterations)
        {
            for (int i = 0; i < iterations; i++)
            {
                var errors = ErrorsGenerator.GenerateErrors();
                if (errors == null)
                {
                    Assert.Fail();
                }
            }

            Assert.Pass();
        }
    }
}
